var app = angular.module('application', []);
app.controller('applicationController', function( $scope, fnc, api ){

	$scope.etiquetas = [
		{ id: 1 , nombre: 'Etiqueta 1' },
		{ id: 2 , nombre: 'Etiqueta 2' },
		{ id: 3 , nombre: 'Etiqueta 3' }
	];

	$scope.tipos = [
		{ id: 1 , nombre: 'Tipo 1' },
		{ id: 2 , nombre: 'Tipo 2' },
		{ id: 3 , nombre: 'Tipo 3' }
	];


	$scope.elementos = [
		{ id: 1, nombre: 'Elemento 1', nombreTipo: 'Tipo 1', correo: 'correo@elementos.com', telefono: '667 123 4567' },
		{ id: 2, nombre: 'Elemento 2', nombreTipo: 'Tipo 2', correo: 'correo@elementos.com', telefono: '668 123 4567' },
		{ id: 3, nombre: 'Elemento 3', nombreTipo: 'Tipo 3', correo: 'correo@elementos.com', telefono: '669 123 4567' }
	];

	$scope.agregar = function(){
		$scope.elemento = {};
		api.modal.open('#modalAgregar');
	};

	$scope.detalle = function( elemento ){
		$scope.elementoDetalle = elemento;
		api.modal.open('#modalDetalle');
	};

});