(function(){
	var app = angular.module('fnc', []);
	app.factory('fnc', function( $http, $timeout, $interval, $location ){
		var fnc = {};

		fnc.defaultSelect = function( array, key, value ){
			var object = {};
			object[key] = value;
			if( array ){
				array.splice(0, 0, object);
				return array;
			}
			return object;
		};

		fnc.url = function(){ return $location.$$url; };

		return fnc;
	});
})();