(function(){

	var app = angular.module('directives', []);

	app.directive('dropdown', ['$rootScope', '$timeout', function($rootScope, $timeout){
		return {
			restrict: 'A',
			link: function($scope, elem, attrs, controller) {
				$(elem).dropdown();
			}
		};
	}]);

	app.directive('dropdownSelect', ['$timeout', function($timeout){
		return {
			scope: { data: '=', trackBy: '@', label: '@', placeholder: '@' },
			require: 'ngModel',
			restrict: 'A',
			templateUrl: 'templates/directives/options.html',
			link: function($scope, elem, attrs, controller) {
				if( !$scope.placeholder ) $scope.placeholder = 'Seleccione...';
				console.log($scope.placeholder);

				if( !attrs.multiple){
					$(elem).dropdown({
						placeholder: $scope.placeholder
					});
				} else {
					$(elem).dropdown('set text', $scope.placeholder);
				}

				$(elem).dropdown();

				$scope.$parent.$watch(attrs.ngModel, function( value ){
					$timeout(function(){
						if( typeof value != 'undefined' ){
							if( value ){
								$(elem).dropdown('set selected', value);
							}
							else{
								$(elem).dropdown('clear')
								.dropdown('restore defaults')
								.dropdown('set text', $scope.placeholder);		
							}
						}
					}, 1);
				});
			}
		};
	}]);

	app.directive('modal', [function(){
		return {
			scope: { modal: '@' },
			restrict: 'A',
			link: function($scope, elem, attrs, controller) {
				$(elem).addClass('ui').addClass('modal').modal();
				if( $scope.modal ) $(elem).attr('id', $scope.modal);
			}
		};
	}]);

	app.directive('pageHeader', [function(){
		return {
			scope: { icon:'@', header:'@', subHeader:'@' },
			restrict: 'AE',
			templateUrl: 'templates/directives/page-header.html'
		};
	}]);

	app.directive('popup', [function(){
		return {
			scope: { position: '@' },
			restrict: 'A',
			link: function($scope, elem, attrs, controller) {
				$(elem).popup({
					position: $scope.position ? $scope.position : 'top left'
				});
			}
		};
	}]);

})();