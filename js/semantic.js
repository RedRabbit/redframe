(function(){
	var app = angular.module('semantic', []);
	app.factory('semantic', function( $http, $timeout, $interval, $location ){
		var semantic = {};

		semantic.dropdown = function( selector ){
			selector = selector ? selector : '.ui.dropdown'
			$(selector).dropdown();
		};

		return semantic;
	});
})();