(function(){

	var app = angular.module('app', ['fnc', 'states', 'directives', 'semantic', 'api']);

	app.controller('appController', function( $scope, fnc, semantic, api ){
		$scope.semantic = semantic;
		$scope.api = api;

		$scope.layout = {};
		$scope.layout.header = 'templates/layout/header.html';
		$scope.layout.aside = 'templates/layout/aside.html';
		$scope.layout.footer = 'templates/layout/footer.html';

		$scope.showSideBar = function(){ $scope.flagSideBar = !$scope.flagSideBar; };
		$scope.setSideBarVisible = function( flag ){ $scope.flagSideBarVisible = flag; };
		$scope.setTheme = function( theme ){ $scope.themeClass = theme; };

		$scope.semanticHeader = function(){
			$('header .item.popup').popup({
				on: 'click',
				transition: 'fade down'
			});
		};
	});

})();