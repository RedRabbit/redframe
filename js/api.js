(function(){
	var app = angular.module('api', []);
	app.factory('api', function( $http, $timeout, $interval, $location ){
		var api = {};

		api.dropdown = function( selector ){
			$(selector).dropdown();
			console.log(selector);
		};

		api.modal = {};
		api.modal.show = api.modal.open = function( selector ){
			$(selector).modal('show');
		};
		api.modal.hide = api.modal.close = function( selector ){
			$(selector).modal('hide');
		};

		return api;
	});
})();