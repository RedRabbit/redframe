(function(){
	var app = angular.module('states', ['ui.router', 'oc.lazyLoad']);
	app.config(function( $urlRouterProvider, $stateProvider ){
		var states = [
			{ url: "inicio", dir: "application/", file: "index", ext: "html" }
		];

		$urlRouterProvider.otherwise("/inicio");

		states.forEach(function( state, index, array ){
			$stateProvider.state(state.url,{
				url: '/'+state.url,
				templateUrl: 'api/'+state.dir+state.file+'.'+state.ext+'?hash'+(new Date).getTime(),
				controller: function($scope, $stateParams){
					$scope.idActividad = $stateParams.idActividad;
					$scope.route = 'api/'+state.dir;
				},
				resolve: {
					include: function($ocLazyLoad){
						return $ocLazyLoad.load({
							name: state.file,
							files: [ 'api/'+state.dir+state.file+'.js' ]
						});
					}
				}
			});
		});
	});
})();